export interface User {
  id: string;
  phone: string;
  username: string;
  first_name: string;
  last_name: string;
  photos: UserPhoto[];

  created_at: Date;
  updated_at: Date;
}

export interface UserPhoto {
  id: string;
  src: string;
  created_at: Date;
  updated_at: Date;
}
