export interface Message {
  _id?: string;
  key: string;
  type: MessageType;
  owner_id: string;
  chat_id: string;
  created_at: Date;
  updated_at: Date;
  data: MessageData;
}

export type MessageType = 'text' | 'image' | 'sticker' | 'url' | 'file' | 'audio' | 'video';

export interface MessageData {
  [i: string]: any;
  text?: string;

  url_src?: string;
  url_img?: string;
  url_title?: string;
  url_desc?: string;

  image_src?: string;
  image_height?: number;
  image_width?: number;

  sticker?: string;

  file_src?: string;
  file_name?: string;
  file_size?: number;

  au_src?: string;
  au_dur?: number;
  au_fft?: Array<number>;

  v_src?: string;
  v_dur?: number;
  v_size?: number;
  v_height?: number;
  v_width?: number;
}
