import { Message } from './message';
export interface Chat {
  _id: string;
  key: string;
  type: ChatType;
  member1: ChatMember;
  member2: ChatMember;
  members: ChatMember[];
  group_title: string;
  group_avatar: string;
  created_at: Date;
  updated_at: Date;
  // messages: Message[];
  message: Message;
}

export type ChatType = 'private' | 'group';

export interface ChatMember {
  user_id: string;
  username: string;
  first_name: string;
  last_name: string;
  avatar: string;
}
