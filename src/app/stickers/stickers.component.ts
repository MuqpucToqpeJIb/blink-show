import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'sticker-place',
    templateUrl: 'stickers.component.html',
    styleUrls: ['stickers.component.scss']
})

export class StickersComponent {
    @Input() hidden;
    @Input() display;
    @Output() hideStick = new EventEmitter();
    @Output() sendMessage = new EventEmitter();
    //public display  = 'flex';

    public hideStickers() {
        this.hideStick.emit({display: 'none', hidden: true});
    }
    public sendSticker(url){
        this.sendMessage.emit({type: 'sticker', data:{sticker: url}})
    }
}