import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-full-screen-layout-profile',
    templateUrl: './full-screen-layout-profile.component.html',
    styleUrls: ['./full-screen-layout-profile.component.scss']
})
export class FullScreenLayoutProfileComponent implements OnInit {

    public isProfile = true;
    public image: string;
    public routeData

    constructor(private route: ActivatedRoute) {
        //this.isProfile = this.route.snapshot.data.isProfile;
    }

    public ngOnInit() {

        console.log(this.isProfile)

        if (this.isProfile) {
            this.image = '/assets/img/gap-logo-w.svg';

        } else {
            this.image = '/assets/img/gap-logo.svg';

        }
    }

}
