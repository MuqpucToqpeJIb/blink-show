import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-full-screen-layout',
    templateUrl: './full-screen-layout.component.html',
    styleUrls: ['./full-screen-layout.component.scss']
})
export class FullScreenLayoutComponent implements OnInit {

    public isProfile = false;
    public image: string;


    constructor(private route: ActivatedRoute) {
        //this.isProfile = this.route.snapshot.data.isProfile;
    }

    public ngOnInit() {

        console.log(this.isProfile)

        if (this.isProfile) {
            this.image = '/assets/img/gap-logo-w.svg';

        } else {
            this.image = '/assets/img/gap-logo.svg';

        }
    }

}
