import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public phone: string;
  public errorMessage: string;
  public phoneCum = {
    code: '+998',
    fisrtCode: '',
    first: '',
    second: '',
    third: ''
  };
  public mask = [ /[0-9]/, /\d/,  ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/];

  constructor(private authService: AuthService, private router: Router) { }

  public ngOnInit() {
    //
  }
  public stripSpaces(phone) {
    let reg = new RegExp('[ ]+', 'g');
    return phone.replace(reg, '');
  }
  public login() {
    console.log(this.phone);
    this.authService.login('998' + this.stripSpaces(this.phone)).then((res) => {
      this.router.navigate(['/confirm']);
    }).catch((reason) => {
      if (reason) {
        this.errorMessage = reason;
      }
    });
  }


}
