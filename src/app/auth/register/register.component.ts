import {Router} from '@angular/router';
import {AuthService} from './../auth.service';
import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    public formData: FormData = new FormData();
    public phone: string;
    public username: string;
    public errorName: boolean = false;
    public errorUName: boolean = false;
    public errorUNameText: string;
    public firstName: string;
    public lastName: string;
    public agree: boolean = false;
    public errorMessage: string;
    public src;

    public get enteredPhone() {
        return localStorage.getItem('phone');
    }

    constructor(private authService: AuthService, private router: Router, private sanitizer: DomSanitizer ) {
    }

    public ngOnInit() {
        //
    }

    public testSub() {

    }

    public register() {
        // check phone format
        // if (!this.agree) {
        //   this.errorMessage = 'Вы должны принять условия публичной оферты';
        //   return;
        // }
        let name;
        for (let i = 0; this.username.length > i; i++) {
            name = this.username.replace(/[A-z0-9\-_]*/g, '');
        }
        if (name.length < 1) {
            this.errorUName = false;
            if (this.username.length < 4) {
                this.errorUName = true
                this.errorUNameText = 'Имя пользывателя должно состоять минимум из 4 символов'
            } else {
                this.errorUName = false
                if (this.firstName === '') {
                    this.errorName = true;
                    return;
                } else {
                    this.formData.append('phone', this.phone);
                    this.formData.append('username', this.username);
                    this.formData.append('firstName', this.firstName);
                    this.formData.append('lastName', this.lastName);
                    this.authService.profileEdit(
                        this.formData
                    )
                        .then(() => {
                            this.router.navigate(['/chat']);
                        }).catch((reason) => {
                        if (reason) {
                            this.errorMessage = reason.message;
                        }
                    });

                }
            }
        } else {
            this.errorUName = true;
            this.errorUNameText = 'Имя ' +
                'пользывателя должно содержать' +
                ' в себе только символы Латинского' +
                ' алфавита, цифры и символы - _';
        }


    }

    public uploadPhoto() {
        document.getElementById('avatarfile').click();
    }

    public appendingFile(e) {
       let files =  e.currentTarget.files;
        console.log(e);
        let src = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(files[0]));
        this.src = src;
        this.formData.append('avatarfile', files[0], files[0].name);

    }

}
