import { AppState } from './../app.service';
import { LoginResult } from './login-result';
import { ApiService } from './../services';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthService {

    public get isAuthenticated() {
        return this.appState.get('authToken') && this.appState.get('userId');
    }
    public get token() {
        return localStorage.getItem('token');
    }

    public requestId: string;

    public get phone() {
        return localStorage.getItem('phone');
    }

    public set phone(p: string) {
        localStorage.setItem('phone', p);
    }

    public get lastName() {
        return localStorage.getItem('lastName');
    }

    public set lastName(p: string) {
        localStorage.setItem('lastName', p);
    }

    public get firstName() {
        return localStorage.getItem('firstName');
    }

    public set firstName(p: string) {
        localStorage.setItem('firstName', p);
    }

    public get username() {
        return localStorage.getItem('username');
    }

    public set username(p: string) {
        localStorage.setItem('username', p);
    }

    public get userId() {
        return localStorage.getItem('userId');
    }

    public set userId(p: string) {
        localStorage.setItem('userId', p);
    }

    public get avatar() {
        return localStorage.getItem('avatar');
    }

    public set avatar(p: string) {
        localStorage.setItem('avatar', p);
    }

    constructor(private apiSrv: ApiService, private appState: AppState) {
    }

    public login(phone: string): Promise<any> {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
        console.log('[LOGIN]', arguments);
        this.phone = phone;
        return this.apiSrv.post('auth', { phone, version: 2 })
            .then((res: LoginResult) => {
                if (!res) {
                    return;
                }

            });
    }

    public profileEdit(data: FormData): Promise<any> {
        return this.apiSrv.post('profile/edit',
            data)
            .then((res) => {

                console.log('profile/edit', res);
                // this.username = username;
                // this.firstName = firstName;
                // this.lastName = lastName;
                this.avatar = res.avatar;

            });
    }

    public profileRead(username: string,
        firstName: string,
        lastName: string): Promise<any> {
        return this.apiSrv.post('profile',
            { phone: this.phone, username, firstName, lastName })
            .then((res) => {
                console.log('profile', res);
                if (!res) {
                    return;
                }
                this.username = res.username;
                this.firstName = res.firstName;
                this.lastName = res.lastName;
            });
    }


    public confirm(code: string): Promise<any> {
        return this.apiSrv.post('auth/confirm', { phone: this.phone, code })
            .then((res) => {

                console.log('Confirm', res);
                if (!res) {
                    return;
                }
                this.apiSrv.token = res.data.access_token;
                this.apiSrv.userId = res.data.user_id;
            });
    }

    public resendSms() {
        this.apiSrv.post('resend-sms')
            .then((res) => {
                if (!res) {
                    return;
                }
                this.appState.set('userId', res.id);
            });
    }

}
