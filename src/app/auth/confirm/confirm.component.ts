import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  public code: string;
  public phone: string;
  public errorMessage: string;
  constructor(private authService: AuthService, private router: Router) {
    this.phone = this.authService.phone;
  }

  public ngOnInit() {
    //
  }

  public resendSms() {
    this.authService.resendSms();
  }

  public confirm() {
    if (!this.code) {
      this.errorMessage = 'Код не может быть пустым';
      return;
    }
    this.authService.confirm(this.code)
      .then((res) => {
        this.router.navigate(['/profile']);
      });
  }
}
