export interface LoginResult {
    status: boolean;
    id: number;
    token: string;
    role: string;
    fname: string;
    lname: string;
    sname: string;
}