import { Injectable } from '@angular/core';
import { Chat } from '../shared/model/index';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { ActiveChatService } from './';

@Injectable()
export class ChatsUiService {

  private _closingChat: Subject<Chat> = new Subject();
  private _openingChat: Subject<Chat> = new Subject();
  private closeTimeout;
  constructor(private activeChatService: ActiveChatService) { }

  public closeChat(chat: Chat) {
    if (!chat) { return; }
    this.closeTimeout = setTimeout(() => {
      this.activeChatService.activeChatId = null;
    }, 1000);
    this._closingChat.next(chat);
  }

  public openChat(chat: Chat) {
    clearTimeout(this.closeTimeout);
    this.activeChatService.activeChatId = chat._id;
    this._openingChat.next(chat);
  }

  public get closingChat(): Observable<Chat> {
    return this._closingChat.asObservable();
  }

  public get openingChat(): Observable<Chat> {
    return this._openingChat.asObservable();
  }

}
