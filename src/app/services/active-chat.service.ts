import { MessagesService } from './messages.service';
import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';
import { ChatsService } from './chats.service';
import { Chat, Message } from '../shared/model';
import * as _ from 'lodash';
import { LoadCommandParams } from '../models/load-command-params';

@Injectable()
export class ActiveChatService {

  public activeChat: Rx.Observable<Chat>;
  public activeChatMessages: Rx.Observable<Message[]>;
  public activeChatWithMessages: Rx.Observable<{ chat: Chat, messages: Message[] }>;

  private _activeChatId: Rx.BehaviorSubject<string> = new Rx.BehaviorSubject<string>(null);
  private _messagesLoadCommand: Rx.Subject<LoadCommandParams>
    = new Rx.Subject<LoadCommandParams>();
  private _messagesLoadObs: Rx.Observable<{ chatId: string, lastMessage: Message, count: number }>;
  constructor(
    private chatsService: ChatsService,
    private messagesService: MessagesService
  ) {
    //
    this.activeChat = Rx.Observable.combineLatest(this._activeChatId, this.chatsService.chats)
      .map((v, i) => {
        let chats = v[1];
        let chatId = v[0];
        let chat = _.find(chats, (ch) => ch._id === chatId); // chats[chatId];
        return chat;
      });

    this.activeChatMessages = Rx.Observable.
      combineLatest(this.activeChat, this.messagesService.messages,
      (activeChat, messages) => {
        // console.log('ACTIVE CHAT MESSAGES', activeChat, messages);
        if (activeChat && messages && messages.length) {
          return _.chain(messages)
            .filter((message: Message) =>
              (message.chat_id === activeChat._id))
            .map((message: Message) => {
              // message.is_ = true;
              return message;
            })
            .sortBy((e) => e.created_at)
            // .reverse()
            .value();
        }
        return [];
      }).startWith([]);

    // this.activeChat
    //   .filter((v) => !!v)
    //   .distinctUntilChanged()
    //   .subscribe((chat) => this.messagesService.addMessage(chat.message));
    // this._messagesLoadCommand.throttle()

    this.activeChatWithMessages = this.activeChat
      .combineLatest(this.activeChatMessages, (chat, messages) => ({ chat, messages }))
      .filter((v) => v.chat && v.chat._id &&
        (v.messages instanceof Array) &&
        v.messages.some((m) => v.chat._id === m.chat_id))
      .distinctUntilChanged((x, y) => x.messages.length === y.messages.length && x.chat._id === y.chat._id)
      .do((v) => console.log('activeChatWithMessages', v));

    this.activeChatMessages
      .combineLatest(this._messagesLoadCommand.throttleTime(500), this.activeChat,
      (messages, req, chat) => {
        let prop = `nomore${req.dir}`;
        return {
          chatId: !chat || chat[prop] ? null : chat._id,
          lastMessage: req.dir === 'up' ? _.first(messages) : _.last(messages),
          req
        };
      })
      // .debounceTime(1000)
      .do((v) => console.log('LOAD MESSAGES CHECK', v))
      .filter((v) => !!(!!v.chatId && v.lastMessage && v.lastMessage._id))
      .distinctUntilChanged((x, y) => x.req.id === y.req.id)
      .do((v) => console.log('LOAD MESSAGES', v))
      .subscribe((v) =>
        this.messagesService.loadMessagesforChat(v.chatId, v.lastMessage._id, v.req.count)
      );

    // INTIAL MESSAGE AUTO LOAD
    this.activeChatMessages.withLatestFrom(this.activeChat,
      (messages, chat) => ({ chat, messages }))
      .do((v) => console.log('INITIAL MESSAGE LOAD CHECK', v))
      .filter((v) => !!(v.chat && v.messages && v.messages.length < 10))
      .distinctUntilChanged((x, y) => x.messages.length === y.messages.length && x.chat._id === y.chat._id)
      .do((v) => console.log('CALL LOAD UP', v.messages.length))
      .subscribe(({ messages }) => {
        this.loadUp();
      });
  }

  public set activeChatId(v: string) {
    this._activeChatId.next(v);
  }

  public loadDown() {
    let req: LoadCommandParams = {
      count: 30,
      dir: 'down',
      id: Date.now().toString()
    }
    this._messagesLoadCommand.next(req);
  }

  public loadUp() {
    let req: LoadCommandParams = {
      count: -30,
      dir: 'up',
      id: Date.now().toString()
    }
    console.log('loadUp', req);
    this._messagesLoadCommand.next(req);
  }

}
