/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SocketManagerService } from './socket-manager.service';

describe('Service: SocketManager', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocketManagerService]
    });
  });

  it('should ...', inject([SocketManagerService], (service: SocketManagerService) => {
    expect(service).toBeTruthy();
  }));
});