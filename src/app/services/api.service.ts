import { Order, RequestFile } from './../orders/order';
import { AppState } from './../app.service';
import { LoginResult } from './../auth/login-result';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import { API_BASE_URL } from './variables';

const ADDRESS_BASE = 'http://api-srv-v2.blink.uz/';

export interface ApiResponse {
    status: boolean;
}

@Injectable()
export class ApiService {
    public baseUrl: string;

    constructor(
        private http: Http,
        @Inject(API_BASE_URL) baseUrl: string
    ) {
        this.baseUrl = baseUrl;
    }

    get token(): string {
        return localStorage.getItem('token');
    }

    set token(t) {
        localStorage.setItem('token', t);
    }

    get userId(): string {
        return localStorage.getItem('userId');
    }

    set userId(u) {
        localStorage.setItem('userId', u);
    }

    public get(apiName: string, search: URLSearchParams = null): Promise<any> {

        let headers = new Headers();
        let p = this.parametrize(headers);

        headers.append('Content-Type', 'application/json');
        return this.http.get(this.baseUrl + apiName, { headers, search })
            .toPromise()
            .then((res) => {
                let r = res.json() as ApiResponse;
                if (r && (r.status || res.ok)) {
                    return Promise.resolve(r);
                }
                return Promise.reject(res);
            })
            .catch(this.handleError);

    }

    public post(apiName: string, body: {} = {}, search: URLSearchParams = null): Promise<any> {

        let headers = new Headers();
        let p = this.parametrize(headers);

        // headers.append('Content-Type', 'application/json');
        return this.http.post(this.baseUrl + apiName, body, { headers, search })
            .toPromise()
            .then((res) => {
                let r = res.json() as ApiResponse;
                if (r && (r.status || res.ok)) {
                    return Promise.resolve(r);
                }
                return Promise.reject(res);
            })
            .catch(this.handleError);

    }

    public postWithFiles(params: {}, files: { name: string, file: File }[]): Observable<any> {

        let formData: FormData = new FormData();
        let xhr: XMLHttpRequest = new XMLHttpRequest();
        // let p = this.parametrize(params);
        for (let k in params) {
            if (params.hasOwnProperty(k)) {
                formData.append(k, params[k]);
            }
        }
        files.forEach((f) => {
            formData.append(f.name, f.file, f.file.name);
        });

        xhr.open('POST', `${this.baseUrl}upload`, true);
        let token = this.token;
        if (token) {
            xhr.setRequestHeader('Authorization', `Bearer ${token}`);
        }
        xhr.send(formData);
        return Observable.create((observer) => {

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {

                    let json = JSON.parse(xhr.response);
                    if (xhr.status === 200 && json.status) {

                        observer.next({ type: 'finish', value: json });
                        observer.complete();

                    } else {
                        this.handleError(json);
                        observer.error(json);
                    }
                }
            };

            xhr.upload.onprogress = (event) => {
                let progress = Math.round(event.loaded / event.total * 100);

                observer.next({ type: 'progress', value: progress });
            };
        });
    }

    private parametrize(headers: Headers) {
        let token = this.token;
        if (token) {
            headers.append('Authorization', `Bearer ${token}`);
        }
    }

    private handleError(error: any): Promise<any> {
        console.log('ERROR', error); // for demo purposes only
        return Promise.reject(error.message || 'Произошла ошибка');
    }
}
