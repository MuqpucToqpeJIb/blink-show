import { Injectable, Inject } from '@angular/core';
import * as io from 'socket.io-client';
import { SocketEvents, SocketCommands } from './socket-events';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../auth';
import { ApiService } from './api.service';
import { SOCKET_BASE_URL } from './variables';
import * as _ from 'lodash';

@Injectable()
export class SocketService {
    public socket: SocketIOClient.Socket;
    public baseAddress;
    // private _listners: Map<String, { func: Function, context?: any }[]> = new Map();
    private _listners2: Map<String, Observable<any>> = new Map();
    private subscribes: Subscription[] = [];
    constructor(
        private apiService: ApiService,
        @Inject(SOCKET_BASE_URL) baseUrl: string,
    ) {
        this.baseAddress = baseUrl;
        this.socket = io.connect(this.baseAddress, { query: { token: this.apiService.token } });
        this.initListeners();
    }

    public initListeners() {
        this.socket.on(SocketEvents.Lifecycle.connected, this.onConnected);
        this.socket.on(SocketEvents.Lifecycle.disconnected, this.onDisconnected);
        this.socket.on(SocketEvents.Lifecycle.error, this.onError);
        this.socket.on(SocketEvents.Lifecycle.reconnect, this.onConnected);
    }

    public onConnected() {
        //
        console.log('CONNECTED');
    }
    public onDisconnected() {
        //
        console.log('DISCONNECTED');

    }
    public onError() {
        console.error(arguments);
    }

    // public on(event: string, fc: { func: Function, context?: any } | Function) {
    //     let fc1: { func: Function, context?: any } = fc instanceof Function ? { func: fc } : fc;
    //     if (this._listners.has(event)) {
    //         let arr = this._listners.get(event);
    //         arr.push(fc1);
    //     } else {
    //         this._listners.set(event, [fc1]);

    //         this.socket.on(event, (...args) => {
    //             console.log('>>>>> [ EVENT: ' + event + ' ]', args);
    //             if (this._listners.has(event)) {
    //                 this._listners.get(event).forEach((f) => f.func.apply(f.context, args));
    //             }
    //         });
    //     }
    // }

    public on<T>(event: string, func: (arg: T) => {}) {
        let obs = this.observable(event);
        this.subscribes.push(obs.subscribe(func));
    }

    public observable<T>(event: string): Observable<T> {
        if (!this._listners2.has(event)) {
            this._listners2.set(event, Observable.fromEvent<T>(this.socket, event));
        }
        let obs = this._listners2.get(event);
        return obs.do((v) => console.log('>>>>> [ EVENT: ' + event + ' ]', v));
    }

    public emit(event: string, ...args) {
        this.onCommand.apply(this, [event, ...args]);
        if (args && args.length) {
            this.socket.emit.apply(this.socket, [event, ...args]);
        } else {
            this.socket.emit(event);
        }
    }

    public dispose() {
        this.subscribes.forEach((s) => s.unsubscribe());
    }

    private onEvent(event: string, ...args: any[]) {
        //
    }

    private onCommand(command: string, ...args: any[]) {
        console.log.apply(console, ['<<<<< [ COMMAND: ' + command + ' ]', ...args]);
    }
}
