import { SocketEvents, SocketCommands } from './socket-events';
import { Chat } from '../shared/model/chat';
import { SocketService } from './socket.service';
import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';
import { MessagesService } from './';


interface IChatOperation extends Function {
  (chats: Chat[]): Chat[];
}

const initialChats: Chat[] = [];

@Injectable()
export class ChatsService {

  // a stream that publishes new chats only once
  newChat: Subject<Chat> = new Subject<Chat>();

  public chats: Observable<Chat[]>;//{ [key: number]: Chat }>;
  public orderedChatsByType: Observable<Chat[]>;
  // action streams
  create: Subject<Chat> = new Subject<Chat>();
  loadList: Subject<Chat[]> = new Subject<Chat[]>();
  noMore: Subject<{ chatId: string, isUp: boolean }>
    = new Subject<{ chatId: string, isUp: boolean }>();
  // `updates` receives _operations_ to be applied to our `chats`
  // it's a way we can perform changes on *all* chats (that are currently
  // stored in `chats`)
  public updates: Subject<any> = new Subject<any>();
  private chatListEvent: Observable<Chat[]>;
  private chatCreatedEvent: Observable<Chat>;
  private _activeChatType: Subject<string> = new BehaviorSubject<string>(null);
  constructor(
    private socketService: SocketService,
    private messageService: MessagesService
  ) {
    this.chats = this.updates
      // watch the updates and accumulate operations on the chats
      .scan((chats: Chat[],
        operation: IChatOperation) => {
        return operation(chats);
      },
      initialChats)
      // make sure we can share the most recent list of chats across anyone
      // who's interested in subscribing and cache the last known list of
      // chats
      .publishReplay(1)
      .refCount();

    this.create
      .map((chat: Chat): IChatOperation => {
        return (chats: Chat[]) => {
          return chats.concat(chat);
        };
      })
      .subscribe(this.updates);

    this.loadList.map((list: Chat[]): IChatOperation => {
      return (chats: Chat[]) => {
        console.log("LOAD CHAT LIST");
        return chats.concat(list);
      };
    }).subscribe(this.updates);

    this.noMore.map(({ chatId, isUp }) => {
      return (chats: Chat[]) => {
        console.log('NO MORE', chatId);
        chats.find((ch) => ch._id === chatId)[`nomore${isUp ? 'up' : 'down'}`] = true;
        return chats;
      }
    }).subscribe(this.updates);

    this.newChat
      .subscribe(this.create);

    this.orderedChatsByType = this.chats.combineLatest(this._activeChatType)
      .map((v) => {
        let chatsDic = v[0];
        let chatType = v[1];
        return _.sortBy(_.toArray(chatsDic)
          .filter((ch) => ch.type === chatType && !!ch.message),
          (ch) => ch.message.created_at).reverse();
      });
    this.setupListeners();
  }

  // an imperative function call to this action stream
  public addChat(chat: Chat): void {
    this.newChat.next(chat);
  }

  public loadChats() {
    this.socketService.emit(SocketCommands.Chat.getAll);
  }

  public createChat(chat: Chat): void {
    this.socketService.emit(SocketCommands.Chat.create, chat);
  }

  public setChatType(type: string) {
    this._activeChatType.next(type);
  }

  private setupListeners() {
    this.chatListEvent = this.socketService.observable(SocketEvents.Chat.chatList);
    this.chatListEvent.subscribe((list) => {
      this.loadList.next(list);
      this.messageService.loadList.next(list.filter((ch) => !!ch.message).map((l) => l.message));
    });

    this.socketService.observable(SocketEvents.Chat.pagination)
      .subscribe(({ messages, request }) => {
        console.log('SUBSCRIBE FROM CHAT SERVICE', SocketEvents.Chat.pagination, messages, request);
        if (Math.abs(request.limit) > messages.length) {
          this.noMore.next({ chatId: request.chat_id, isUp: request.limit < 0 });
        }
      });
    this.chatCreatedEvent = this.socketService.observable(SocketEvents.Chat.created);
    this.chatCreatedEvent.subscribe(this.newChat);
  }

}
