import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { SocketEvents, SocketCommands } from './socket-events';
import { Observable, Subscription } from 'rxjs';

@Injectable()
export class SocketManagerService {

  public socket: SocketIOClient.Socket;
  public baseAddress: string = 'socket-srv.blink.uz:9092';
  constructor() { }

  public subscribeOnEvents() {

  }

  public generateCommands() {

  }

}