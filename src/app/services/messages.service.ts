import { SocketService } from './';
import { SocketCommands, SocketEvents } from './socket-events';
import { User } from '../shared/model/user';
import { Chat } from '../shared/model/chat';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Message } from '../shared/model';
import * as _ from 'lodash';

interface IOperationMessage extends Function {
  (list: Message[]): Message[];
}

const initialMessages: Message[] = [];


@Injectable()
export class MessagesService {
  // a stream that publishes new messages only once
  newMessages: Subject<Message> = new Subject<Message>();

  // `messages` is a stream that emits an array of the most up to date messages
  messages: Observable<Message[]>;

  loadList: Subject<Message[]> = new Subject<Message[]>();
  // `updates` receives _operations_ to be applied to our `messages`
  // it's a way we can perform changes on *all* messages (that are currently
  // stored in `messages`)
  updates: Subject<any> = new Subject<any>();

  // action streams
  create: Subject<Message> = new Subject<Message>();
  markChatAsRead: Subject<any> = new Subject<any>();
  private messageListEvent: Observable<{ messages: Message[], [key: string]: any }>;
  private messageCreatedEvent: Observable<Message>;
  constructor(
    private socketService: SocketService
  ) {


    this.messages = this.updates
      // watch the updates and accumulate operations on the messages
      .scan((messages: Message[], operation: IOperationMessage) => {
        // console.log('NEW UPDATE FOR MESSAGE');
        return operation(messages);
      }, initialMessages)
      .startWith(initialMessages)
      // make sure we can share the most recent list of messages across anyone
      // who's interested in subscribing and cache the last known list of
      // messages
      .publishReplay(1)
      .refCount();

    this.loadList.map((list: Message[]): IOperationMessage => {
      // console.log('MESSAGES LIST LOADED', list);
      return (msgs: Message[]) => {
        return this.concatMessages(msgs, list);
      };
    }).subscribe((oper) => {
      // console.log('NEW LIST LOAD OPERATION');
      this.updates.next(oper);
    });
    // `create` takes a Message and then puts an operation (the inner function)
    // on the `updates` stream to add the Message to the list of messages.
    //
    // That is, for each item that gets added to `create` (by using `next`)
    // this stream emits a concat operation function.
    //
    // Next we subscribe `this.updates` to listen to this stream, which means
    // that it will receive each operation that is created
    //
    // Note that it would be perfectly acceptable to simply modify the
    // "addMessage" function below to simply add the inner operation function to
    // the update stream directly and get rid of this extra action stream
    // entirely. The pros are that it is potentially clearer. The cons are that
    // the stream is no longer composable.
    this.create
      .map((message: Message): IOperationMessage => {
        return (messages: Message[]) => {
          console.log(messages);
          return messages.concat(message);
        };
      })
      .subscribe(this.updates);

    this.newMessages
      .subscribe(this.create);

    // similarly, `markChatAsRead` takes a Chat and then puts an operation
    // on the `updates` stream to mark the Messages as read
    this.markChatAsRead
      .map((chat: Chat) => {
        return (messages: Message[]) => {
          return messages.map((message: Message) => {
            // note that we're manipulating `message` directly here. Mutability
            // can be confusing and there are lots of reasons why you might want
            // to, say, copy the Message object or some other 'immutable' here
            if (message.chat_id === chat._id) {
              // message.isRead = true; TODO
            }
            return message;
          });
        };
      })
      .subscribe(this.updates);

    this.setupListeners();
  }

  // an imperative function call to this action stream
  public addMessage(message: Message): void {
    console.log(message);
    this.socketService.emit(SocketCommands.Messages.Message.create, message); //Test added row, may be wrong.
    this.newMessages.next(message);
  }

  public loadMessagesforChat(
    chatId: string,
    messageId: string,
    // from: Date,
    // to: Date,
    limit: number = 50) {
    this.socketService.emit(SocketCommands.Messages.pagination,
      {
        chat_id: chatId,
        message_id: messageId,
        // from_date: from,
        // to_date: to,
        limit
      }
    );
  }

  messagesForChatUser(chat: Chat, user: User): Observable<Message> {
    return this.newMessages
      .filter((message: Message) => {
        // belongs to this Chat
        return (message.chat_id === chat._id) &&
          // and isn't authored by this user
          (message.owner_id !== user.id);
      });
  }
  private setupListeners() {
    this.messageListEvent = this.socketService.observable(SocketEvents.Chat.pagination);
    this.messageListEvent.subscribe(({ messages, request }) => {
      if (messages && messages.length) {
        this.loadList.next(messages);
      }
    });
    this.messageCreatedEvent = this.socketService.observable(SocketEvents.Messages.Message.created);
    this.messageCreatedEvent.subscribe(this.create);
    // console.log('subscribe ON MESSAGES');
    // this.socketService.on(SocketEvents.Chat.pagination, (...args) => {
    //   console.log(args);
    // })
  }

  private concatMessages(msgs: Message[], newMsgs: Message[]) {
    return _.chain(msgs).concat(newMsgs).uniqBy((v, i, m) => v._id).value();
  }
}
