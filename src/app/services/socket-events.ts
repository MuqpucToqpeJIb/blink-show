
export const SocketCommands = {
  Users: {
    request: 'users:request',
    block: 'user:block',
    unblock: 'user:unblock',
  },

  Chat: {
    create: 'chat:create',
    getAll: 'chats:request'
  },

  Messages: {
    Message: {
      create: 'message:create',
      update: 'message:update',
      delete: 'message:delete'
    },
    request: 'messages:request',
    read: 'messages:read',
    pagination: 'messages:request:pagination',
    markAsRead: 'messages:view',
    initial: 'messages:initial',
  },
  Group: {
    create: 'group:create',
    update: 'group:update',
    delete: 'group:delete',
    updateTitle: 'group:title:update',
    updateAvatar: 'group:avatar:update',
    addMembers: 'group:members:add',
    removeMembers: 'group:members:remove',
  },

  Action: {
    typing: 'action:typing',
    none: 'action:none',
    sending: 'action:sending',
  }
};
export const SocketEvents = {

  Lifecycle: {
    connected: 'connect',
    error: 'error',
    reconnect: 'reconnect',
    reconnectAttempt: 'reconnectAttempt',
    disconnected: 'disconnect',
  },

  Users: {
    contacts: 'users:request',
    blocked: 'user:blocked',
    unblocked: 'user:unblocked'
  },

  Chat: {
    created: 'chat:created',
    chatList: 'chats',
    messages: 'chat:messages',
    initial: 'chat:initial',
    pagination: 'chat:messages:pagination',
  },

  Messages: {
    Message: {
      created: 'message',
      updated: 'message:updated',
      deleted: 'message:deleted'
    },
    read: 'messages:readed',
    viewed: 'messages:viewed',
  },

  Group: {
    created: 'group:created',
    updated: 'group:updated',
    deleted: 'group:deleted',
    updatedTitle: 'group:title:updated',
    updatedAvatar: 'group:avatar:updated',
    membersAdded: 'group:members:added',
    membersRemoved: 'group:members:removed',
  },

  Action: {
    typing: 'action:typing',
    none: 'action:none',
    sending: 'action:sending',
  }
};
