/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActiveChatService } from './active-chat.service';

describe('Service: ActiveChat', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActiveChatService]
    });
  });

  it('should ...', inject([ActiveChatService], (service: ActiveChatService) => {
    expect(service).toBeTruthy();
  }));
});