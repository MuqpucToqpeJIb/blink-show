import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class LightboxServices{
    private subject = new Subject<any>();

    public openLightBox(src: string, isVideo: boolean) {
        this.subject.next({src, isVideo});
    }
    public openLightBoxNext(): Observable<any>{
        return this.subject.asObservable();
    }
    public clearImg(){
        this.subject.next();
    }
}