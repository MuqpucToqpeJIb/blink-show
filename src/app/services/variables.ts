import { InjectionToken } from '@angular/core';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');
export const SOCKET_BASE_URL = new InjectionToken<string>('SOCKET_BASE_URL');
export const CDN_BASE_URL = new InjectionToken<string>('CDN_BASE_URL');
