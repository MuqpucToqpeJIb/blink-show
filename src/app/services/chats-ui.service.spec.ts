/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ChatsUiService } from './chats-ui.service';

describe('Service: ChatsUi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatsUiService]
    });
  });

  it('should ...', inject([ChatsUiService], (service: ChatsUiService) => {
    expect(service).toBeTruthy();
  }));
});