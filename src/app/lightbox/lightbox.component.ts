import { AfterViewInit, Component, DoCheck, EventEmitter, OnInit, Output } from '@angular/core';
import { LightboxServices } from '../services/lightbox-services';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'light-box',
    templateUrl: 'lightbox.component.html',
    styleUrls: ['lightbox.component.scss']
})

export class LightboxComponent implements OnInit{
    public src;
    @Output() hideLightbox = new EventEmitter();
    public subscription: Subscription;
    public isVideo: boolean;
    constructor(private lightboxService: LightboxServices) {
        this.subscription = this.lightboxService.openLightBoxNext().subscribe((data) => {
            this.src = data.src;
            this.hideLightbox.emit(true);
            this.isVideo = data.isVideo;
            setTimeout(()=>{
                this.centerImage();
            }, 30);

        });
    }

    public ngOnInit() {
        document.addEventListener('keyup', (e)=>{
            if(e.keyCode === 27) this.closeImg(false);
        })
    }

    public centerImage() {
        let image = document.getElementById('img-lightBox') || document.getElementById('video-lightBox');
        image.style.marginTop = "-" + image.clientHeight / 2 + "px";
        image.style.marginLeft = "-" + image.clientWidth / 2 + "px";
        console.log(image.clientWidth);
    }

    closeImg(state) {
        this.hideLightbox.emit(state);
        this.src = null;
        //this.lightboxService.clearImg();
    }


}
