import { Component, Input, OnInit } from '@angular/core';
import { MessageBaseComponent } from '../message-base.component';
import { Message } from '../../shared/model';

@Component({
  selector: 'text-message',
  templateUrl: './text-message.component.html',
  styleUrls: ['./text-message.component.scss']
})
export class TextMessageComponent implements OnInit, MessageBaseComponent {
  @Input()
  public message: Message;
  constructor() {
    //
  }

  public ngOnInit() {
    //
  }

}
