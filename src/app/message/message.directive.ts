import {
    Directive, ViewContainerRef, ComponentFactoryResolver, Input,
    OnDestroy
} from '@angular/core';
import { Message } from '../shared/model';
import { TextMessageComponent } from './text-message/text-message.component';
import { ImgMessageComponent } from './img-message/img-message.component';
import { SoundMessageComponent } from './sound-message/sound-message.component';
import { MessageBaseComponent } from './message-base.component';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { VideoMessageComponent } from './video-message/video-message.component';
import { StickerMessagesComponent } from './sticker-messages/sticker-messages.component';

const messageTypes = {
  text: TextMessageComponent,
  image: ImgMessageComponent,
  audio: SoundMessageComponent,
  video: VideoMessageComponent,
  sticker: StickerMessagesComponent
};

@Directive({
  selector: '[message]'
})

export class MessageDirective implements OnInit, OnDestroy {

  @Input()
  public message: Message;
  constructor(
    private _vc: ViewContainerRef,
    private _cfr: ComponentFactoryResolver) { }

  public ngOnDestroy(): void {
    // throw new Error("Method not implemented.");
  }
  public ngOnInit(): void {
    console.log('MESSAGE', this.message);
    let comp = messageTypes[this.message.type];
    let componentFactory = this._cfr.resolveComponentFactory(comp);
    this._vc.clear();
    let componentRef = this._vc.createComponent(componentFactory);
    (<MessageBaseComponent>componentRef.instance).message = this.message;
  }

}
