import { Component, Input, OnInit } from '@angular/core';
import { MessageBaseComponent } from '../message-base.component';
import { LightboxServices } from '../../services/lightbox-services';
import { Message } from '../../shared/model';

@Component({
    selector: 'video-message',
    templateUrl: './video-message.component.html',
    styleUrls: ['./video-message.component.scss']
})
export class VideoMessageComponent implements OnInit, MessageBaseComponent {
    public myVideo;
    public url = 'http://api-srv-v2.blink.uz';
    public videoWidth;
    public src;
    @Input() own;
    @Input() message: Message;
    constructor(private lightboxService: LightboxServices) {
    }

    public ngOnInit() {
        this.generateThumbUrl();
        let videoWidth = document.getElementById('video-block');
        setTimeout(()=>{
            this.videoWidth = videoWidth.clientWidth;
            console.log(this.videoWidth);
        }, 300)


    }

    playVideo(e) {
        // console.log(e.target.children);
        // e.target.classList.add('played');

        this.lightboxService.openLightBox(e, true);
    }

    public generateThumbUrl(){
        let indexOfFormat = this.message.data.v_src.lastIndexOf('.');
        let urlWithoutFormat = (this.message.data.v_src as any).slice(0, -(this.message.data.v_src.length-indexOfFormat));
        urlWithoutFormat = urlWithoutFormat + ".jpeg";
        let arrayPath = urlWithoutFormat.split('/');
        console.log(arrayPath);
        arrayPath[4] = arrayPath[3];
        arrayPath[3] = "thmb";
        console.log(arrayPath);
        let url = '';
        for(let i=0; i<arrayPath.length; i++ ){

            url = url + arrayPath[i];
            if(i != arrayPath.length -1){
                url= url + '/';
            }

        }
        console.log('newUrl', url);
        this.src = this.url + url;
    }

}
