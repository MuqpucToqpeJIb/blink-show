import { Component, Input, OnInit } from '@angular/core';
import { MessageBaseComponent } from '../message-base.component';

@Component({
    selector: 'sticker-message',
    templateUrl: 'sticker-messages.component.html',
    styleUrls: ['sticker-messages.component.scss']
})
export class StickerMessagesComponent implements OnInit, MessageBaseComponent {
    @Input() message;
    public sticker;

    public ngOnInit() {
        this.sticker = 'http://api-srv-v2.blink.uz' + this.message.data.sticker;
    }

}