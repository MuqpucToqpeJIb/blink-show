import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { MessageBaseComponent } from '../message-base.component';
import { Message } from '../../shared/model';

@Component({
    selector: 'sound-message',
    templateUrl: './sound-message.component.html',
    styleUrls: ['./sound-message.component.scss']
})
export class SoundMessageComponent implements OnInit, DoCheck, MessageBaseComponent {
    @Input() message: Message;
    @Input() imgSrc;
    public file;
    @Input() innerClass;
    @Input() duration;
    public audio;
    public isPlayed = false;
    public wasPlayed = 0;
    public maxRange;
    public shownMax;
    constructor() {
        //
    }

    public ngOnInit() {
        this.file = 'http://api-srv-v2.blink.uz' + this.message.data.au_src;
        this.maxRange = Math.round(this.message.data.au_dur );
        this.audio = new Audio('http://api-srv-v2.blink.uz' + this.message.data.au_src);
        // this.duration = this.audio.duration;
        console.log(this.duration);
        this.shownMax = Math.round(this.message.data.au_dur/1000 )

    }

    public changePosition() {
        this.audio.currentTime = this.wasPlayed/1000;
    }
    public playMusic(src) {
        this.audio = new Audio(src);
        if (this.wasPlayed == 0) {
            this.audio.currentTime = 0;
        } else {
            this.audio.currentTime = this.wasPlayed;
        }
        this.audio.play();
        this.audio.addEventListener('timeupdate', ()=>{
            this.wasPlayed = Math.floor(this.audio.currentTime * 1000);
            console.log(this.audio.currentTime* 1000);
        });

        // this.duration = Math.floor(this.audio.duration);
        this.isPlayed = true;
       // this.setCurent();

    }

    public pauseMusic() {
        this.audio.pause();
        this.isPlayed = false;
        console.log(this.audio.currentTime);
        this.wasPlayed = this.audio.currentTime;
    }
    public ngDoCheck() {
        this.audio.addEventListener('ended', () => {
            this.audio = new Audio();
            this.wasPlayed = 0;
            this.isPlayed = false;
        });




    }

    public setCurent() {
        setTimeout(() => {
            this.wasPlayed = this.audio.currentTime;
            this.setCurent();
        }, 1000);
    }

}
