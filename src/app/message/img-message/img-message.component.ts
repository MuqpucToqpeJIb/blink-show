import { Component, Input, OnInit } from '@angular/core';
import { MessageBaseComponent } from '../message-base.component';
import { Message } from '../../shared/model';
import { LightboxServices } from '../../services/lightbox-services';
import { CDN_BASE_URL } from '../../services/variables';


@Component({
    selector: 'img-message',
    templateUrl: './img-message.component.html',
    styleUrls: ['./img-message.component.scss']
})
export class ImgMessageComponent implements OnInit, MessageBaseComponent {
    @Input() message: Message;

    public url = 'http://api-srv-v2.blink.uz/';
    public src =this.url + "/thmb/";
    public imagesClicked = [];
        constructor(private lightboxService: LightboxServices) {
        //
    }

    public ngOnInit() {
        //localStorage.setItem('image', false);
        this.generateThumbUrl();
        if(localStorage.getItem('image') ){
            this.imagesClicked = JSON.parse(localStorage.getItem('image'));
        }else{
            this.imagesClicked = [];
        }
        if(this.imagesClicked.indexOf(this.message._id) != -1){
            this.src = this.url + this.message.data.image_src;
        }
        console.log('image', this.imagesClicked);
    }


    public openLightboxes(src, id) {

            if( this.imagesClicked.indexOf(id) == -1){
            this.imagesClicked.push(id);
            this.src = this.url + this.message.data.image_src;
            console.log( this.imagesClicked);
            localStorage.setItem('image', JSON.stringify(this.imagesClicked));
        }else{
            console.log('already exist');
        }
        this.lightboxService.openLightBox(src, false);
    }

    public generateThumbUrl(){
        let arrayPath = this.message.data.image_src.split('/');
        console.log(arrayPath);
        arrayPath[4] = arrayPath[3];
        arrayPath[3] = "thmb";

        console.log(arrayPath);
        let url = '';
        for(let i=0; i<arrayPath.length; i++ ){

            url = url + arrayPath[i];
            if(i != arrayPath.length -1){
                url= url + '/';
            }

        }
        console.log(url);
        this.src = this.url + url;
    }
}
