import { Chat } from '../shared/model';

export function chatAvatar(chat: Chat, userId): string {
  if (!chat) { return '/assets/img/avatar.svg'; };

  if (chat.type === 'group') {
    if (chat.group_avatar) {
      return `http://api-srv.blink.uz/${chat.group_avatar}`;
    }
    return '/assets/img/avatar.svg';
  }

  let member = null;

  if (chat.member1.user_id === userId) {
    member = chat.member2;
  } else {
    member = chat.member1;
  }

  if (member && member.avatar) {
    return `http://api-srv.blink.uz/${member.avatar}`;
  } else {
    return '/assets/img/avatar.svg';
  }
}
export function chatName(chat: Chat, userId): string {
  if (!chat) { return ''; };
  if (chat.type === 'group') {
    return chat.group_title;
  }

  if (chat.member1.user_id === userId) {
    return `${chat.member2.first_name} ${chat.member2.last_name}`;
  } else {
    return `${chat.member1.first_name} ${chat.member1.last_name}`;
  }
}
