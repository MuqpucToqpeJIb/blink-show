
export interface LoadCommandParams {
  count: number;
  dir: 'up' | 'down';
  id: string;
}