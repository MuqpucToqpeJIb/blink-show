import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
    NgModule,
    ApplicationRef, NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';

import {
  LoginComponent,
  RegisterComponent,
  ConfirmComponent,
  AuthService
} from './auth';
import {
  FullScreenLayoutComponent
} from './layout';
/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';

import {
  SocketService,
  ChatsService,
  ActiveChatService,
  MessagesService,
  ApiService
} from './services';

import { NgSlimScrollModule } from 'ngx-slimscroll';

import '../styles/styles.scss';
import '../styles/headings.css';
import { MainSidebarComponent } from './home/main-sidebar/main-sidebar.component';
import { ChatsComponent } from './home/chats/chats.component';
import { MainAreaComponent } from './home/main-area/main-area.component';
import { ToolbarComponent } from './home/toolbar/toolbar.component';
import { TextMaskModule } from 'angular2-text-mask';
import { PhonePipe } from './pipes/phone.pipe';
import { AuthGuard } from './auth/auth-guard.service';
import { MessageBubleComponent } from './home/message-types/message-bubble/message-buble.component';
import { TextMessageComponent } from './message/text-message/text-message.component';
import { ImgMessageComponent } from './message/img-message/img-message.component';
import { SoundMessageComponent } from './message/sound-message/sound-message.component';
import {
  FullScreenLayoutProfileComponent
} from './layout/full-screen-layout-profile/full-screen-layout-profile.component';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { VideoMessageComponent } from './message/video-message/video-message.component';

import { NouisliderModule } from 'ng2-nouislider';
import { StickersComponent } from './stickers/stickers.component';
import { LightboxComponent } from './lightbox/lightbox.component';
import { LightboxServices } from './services/lightbox-services';
import { ChatsUiService } from './services/chats-ui.service';
import { CommonModule } from '@angular/common';
import { MessageDirective } from './message/message.directive';
import { StickerMessagesComponent } from './message/sticker-messages/sticker-messages.component';
import { DatePipe } from './pipes/date.pipe';


// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  ApiService,
  AuthService,
  SocketService,
  ChatsService,
  MessagesService,
  ActiveChatService,
  ChatsUiService
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomeComponent,
    NoContentComponent,
    LoginComponent,
    RegisterComponent,
    ConfirmComponent,
    MainSidebarComponent,
    FullScreenLayoutComponent,
    FullScreenLayoutProfileComponent,
    ChatsComponent,
    MainAreaComponent,
    ToolbarComponent,
    PhonePipe,
    DatePipe,
    StickersComponent,
    TextMessageComponent,
    ImgMessageComponent,
    SoundMessageComponent,
    VideoMessageComponent,
    LightboxComponent,
    MessageDirective,
    StickerMessagesComponent
  ],
  /**
   * Import Angular's modules.
   */
  entryComponents: [
      TextMessageComponent,
      ImgMessageComponent,
      SoundMessageComponent,
      VideoMessageComponent,
      StickerMessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpModule,
    BrowserAnimationsModule,
    NgSlimScrollModule,
    InfiniteScrollModule,
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
    TextMaskModule,
    NouisliderModule
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS,
    AuthGuard,
    LightboxServices,
      MessageDirective
  ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) { }

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    /**
     * Set state
     */
    this.appState._state = store.state;
    /**
     * Set input values
     */
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    /**
     * Save state
     */
    const state = this.appState._state;
    store.state = state;
    /**
     * Recreate root elements
     */
    store.disposeOldHosts = createNewHosts(cmpLocation);
    /**
     * Save input values
     */
    store.restoreInputValues = createInputTransfer();
    /**
     * Remove styles
     */
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    /**
     * Display new elements
     */
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
