import { CanActivate, Routes } from '@angular/router';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';
import { FullScreenLayoutComponent } from './layout';
import {
    LoginComponent,
    RegisterComponent,
    ConfirmComponent
} from './auth';

import { DataResolver } from './app.resolver';
import { AuthGuard } from "./auth/auth-guard.service";
import { FullScreenLayoutProfileComponent } from './layout/full-screen-layout-profile/full-screen-layout-profile.component';

export const ROUTES: Routes = [
    {
        path: '', component: FullScreenLayoutComponent,
        children: [
            {path: 'login', component: LoginComponent},
            {path: 'confirm', component: ConfirmComponent}
        ]
    },
    {
        path: '', component: FullScreenLayoutProfileComponent,
        children: [
            {path: 'profile', data: {isProfile: true}, component: RegisterComponent}
        ]
    },
    {path: 'chat', component: HomeComponent, canActivate: [AuthGuard]},
    {path: '**', component: NoContentComponent},
];
