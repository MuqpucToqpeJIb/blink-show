import {
    Component,
    DoCheck,
    Input,
    OnInit,
    Output,
    ViewEncapsulation,
    EventEmitter,
    ElementRef,
    ViewChild,
    trigger,
    transition,
    style,
    animate
} from '@angular/core';
import {Message, MessageType} from '../../shared/model/message';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs/Observable';
import { Chat } from '../../shared/model/chat';
import { ActiveChatService } from '../../services/active-chat.service';
import { MessagesService } from '../../services';
import { chatAvatar, chatName } from '../../utils/chat.utils';
import { ChatsUiService } from '../../services/chats-ui.service';

@Component({
    selector: 'main-area',
    templateUrl: 'main-area.component.html',
    styleUrls: ['main-area.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [
        trigger('fadeIn', [
            transition(':enter', [
                style({ opacity: '0' }),
                animate('.5s ease-out', style({ opacity: '1' })),
            ]),
        ]),
    ],
})

export class MainAreaComponent implements OnInit, DoCheck {

    public activeChatName: string = '';
    public activeChatAvatar: string = '';
    public activeChat: Chat;
    // public messages: Message[] = [];
    public someRange = 0;
    public someTest;
    @Input()
    public chatId;
    @Output()
    public closingChat = new EventEmitter();
    @ViewChild('messageBox')
    public messageBox: ElementRef;
    private scrollMsgBox: Observable<any>;
    constructor(
        private chatsUiservice: ChatsUiService,
        private authService: AuthService,
        private activeChatService: ActiveChatService,
        private messagesService: MessagesService
    ) {
        this.activeChatService.activeChat.subscribe((chat) => {
            this.activeChat = chat;
           this.chatId = chat._id;
            this.activeChatName = chatName(chat, this.authService.userId);
            this.activeChatAvatar = chatAvatar(chat, this.authService.userId);

        });

        this.activeChatService.activeChatWithMessages.debounceTime(50)
            .distinctUntilChanged((x, y) => (x.chat && y.chat && x.chat._id === y.chat._id))
            .subscribe((v) => {
                if (this.messageBox && v.messages.length > 1) {
                    //  setTimeout(() => {
                    let scrollTop = v.chat['scrollTop'] !== undefined ?
                        v.chat['scrollTop'] :
                        this.messageBox.nativeElement.scrollHeight;
                    console.log('SCROLL TO DOWN', scrollTop, v.chat);
                    this.messageBox.nativeElement.scrollTo(0, scrollTop);
                    // }, 50);
                }
            });
    }

    public ngOnInit() {
        this.scrollMsgBox = Observable.fromEvent(this.messageBox.nativeElement, 'scroll');
        this.scrollMsgBox.debounceTime(500).subscribe(this.onScroll.bind(this));
        console.log('chatId', this.chatId)
    }

    public ngDoCheck() {
        //
    }
    public changeRange() {
        this.someRange = this.someTest;
    }
    public changedRange() {
        console.log("i'm changed");
    }

    public get messages() {
        return this.activeChatService.activeChatMessages;
    }

    public msgClass(msg: Message) {
        return !msg.owner_id ?
            'system' :
            msg.owner_id === this.authService.userId ? 'own' : 'alien';
    }

    public closeChat() {
        this.closingChat.emit(false);
    }

    public onScrollDown() {
        console.log('SCROLL DOWN');
        this.activeChatService.loadDown();
    }
    public onScrollUp() {
        console.log('SCROLL UP');
        this.activeChatService.loadUp();
    }

    public onScroll(ev) {
        if (!this.messageBox || !this.activeChat) {
            return;
        }
        let el = this.messageBox.nativeElement;
        // console.log('SCROLLLLLLLLLL', el.scrollTop, el.scrollHeight - el.clientHeight, this.activeChat);
        if (this.activeChat['scrollTop'] === undefined &&
            (el.scrollTop === (el.scrollHeight - el.clientHeight))) {
            return;
        }

        console.log('SCROLLLLLLLLLL', el.scrollTop, el.scrollHeight - el.clientHeight, this.activeChat);
        this.activeChat['scrollTop'] = el.scrollTop;
    }

    public sendMessage(e){

        let message = {
            key: 'idontknowwhereiskey', // TODO: insert KEY
            type: e.type,
            owner_id: localStorage.getItem('userId'),
            chat_id: this.chatId,
            created_at: new Date(),
            updated_at: new Date(),
            data: e.data
        };
        this.messagesService.addMessage(message);
    }

}

    // public get activeChat(): Observable<Chat> {
    //     return this.activeChatService.activeChat;
    // }
    // public get activeChatAvatar() {
    //     return this.activeChat.map((chat) => this.chatAvatar(chat));
    // }

    // public get activeChatName() {
    //     return this.activeChat.map((chat) => this.chatName(chat));
    // }
