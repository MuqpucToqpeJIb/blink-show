import { Message } from '../shared/model/message';
import { AuthService } from '../auth/auth.service';
import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  AfterContentInit
} from '@angular/core';
import * as autosize from 'autosize';
import { AppState } from '../app.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';
import { ChatsService, ActiveChatService } from '../services';
import { Observable } from 'rxjs';
import { Chat } from '../shared/model';

@Component({
  selector: 'home',
  providers: [
    Title
  ],
  styleUrls: ['./home.component.scss'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  public sidebarOpened;
  public chatOpened = false;
  public chatId: string = null;
  public show = false;

  constructor(
    public title: Title,
    private activeChatSevice: ActiveChatService,
    private authService: AuthService,
  ) {

  }

  public ngOnInit() {
    this.activeChatSevice.activeChat.subscribe((chat) => {
      if (this.chatOpened === !!chat) { return; }
      this.chatOpened = !!chat;
    });
  }

  public toggleSidebar($event) {
    this.sidebarOpened = $event;
  }

  public openChat(event) {
    this.chatOpened = event.chatOpened;
    this.activeChatSevice.activeChatId = event.id;
    console.log('new chat', event.id)
  }

  public isShown(e) {
    this.show = e;
    console.log(e)
  }

}
