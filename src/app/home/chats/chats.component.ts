import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Chat } from '../../shared/model/chat';
import { Observable } from 'rxjs/Observable';
import { ActiveChatService } from '../../services/active-chat.service';
import { ChatsService } from '../../services/chats.service';
import { AuthService } from '../../auth/auth.service';
import { Message } from '../../shared/model/message';
import { chatAvatar, chatName } from '../../utils/chat.utils';
import { ChatsUiService } from '../../services/chats-ui.service';

@Component({
    selector: 'chats',
    templateUrl: 'chats.component.html',
    styleUrls: ['chats.component.scss']
})

export class ChatsComponent implements OnInit {
    public opts;
    public chatId: string = null;
    public chat: Chat;
    public chatOpened: boolean;
    public chatType: string = 'private';
    public sideBarOpened: boolean = false;
    public get activeChat(): Observable<Chat> {
        return this.activeChatService.activeChat;
    }
    public get chats(): Observable<Chat[]> {
        return this.chatsService.orderedChatsByType;
    }
    @Output() togglingSidebar = new EventEmitter();
    @Output() goingToChat = new EventEmitter();

    constructor(
        private chatUiService: ChatsUiService,
        private activeChatService: ActiveChatService,
        private chatsService: ChatsService,
        private authService: AuthService,
    ) {
        this.activeChat.subscribe((chat) => {
            this.chat = chat;
            if (chat) {
                this.chatId = chat._id;
            }
        });

        this.opts = {
            position: 'right', // left | right
            barBackground: '#5347a4', // #C9C9C9
            barOpacity: '0.7', // 0.8
            barWidth: '6', // 10
            barBorderRadius: '5', // 20
            barMargin: '0', // 0
            gridBackground: '#f0f2f5', // #D9D9D9
            gridOpacity: '1', // 1
            gridWidth: '6', // 2
            gridBorderRadius: '0', // 20
            gridMargin: '10', // 0
            alwaysVisible: true, // true
            visibleTimeout: 2000 // 1000
        }
    }

    public ngOnInit() {
        setTimeout(() => {
            this.chatsService.loadChats();
            this.chatsService.setChatType('private');
        }, 1000);
    }
    public toggleSidebar() {
        this.sideBarOpened = !this.sideBarOpened;
        this.togglingSidebar.emit(this.sideBarOpened);
        this.sideBarOpened = false;
    }
    public get activeChatAvatar() {
        return this.activeChat.map((chat) => chatAvatar(chat, this.authService.userId));
    }

    public chatAvatar(chat) {
        return chatAvatar(chat, this.authService.userId);
    }

    public get messages(): Observable<Message[]> {
        return this.activeChatService.activeChatMessages;
    }

    public gotoChat(id: string) {
        this.goingToChat.emit({ id, chatOpened: true });
        // this.chatOpened = true;
        // this.activeChatService.activeChatId = id;
    }

    public openChat(chat: Chat) {
        this.chatUiService.closeChat(this.chat);
        this.chatUiService.openChat(chat);
    }

    public closeChat() {
        this.chatOpened = false;
    }

    public setChatType(type: string) {
        this.chatType = type;
        this.chatsService.setChatType(type);
    }

    public get activeChatName() {
        return this.activeChat.map((chat) => this.chatName(chat));
    }
    public chatName(chat: Chat) {
        return chatName(chat, this.authService.userId);
    }
}