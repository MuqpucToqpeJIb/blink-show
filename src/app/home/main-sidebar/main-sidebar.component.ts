import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'main-sidebar',
    templateUrl: 'main-sidebar.component.html',
    styleUrls: ['main-sidebar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class MainSidebarComponent{

}
