import {Component, ViewChild, ElementRef, AfterContentInit, Output, EventEmitter} from '@angular/core';

import * as autosize from 'autosize';
import {MessagesService} from "../../services/messages.service";
import {MessageType} from "../../shared/model/message";

@Component({
    selector: 'toolbar',
    templateUrl: 'toolbar.component.html',
    styleUrls: ['toolbar.component.scss'],
    providers: [MessagesService]
})



export class ToolbarComponent implements AfterContentInit {
    public hidden = true;
    public display;
    public message = {
    key: 'string',
    type: ('text' as MessageType),
    owner_id: localStorage.getItem('userId'),
    chat_id: "583d455f6431498cca7336cf",
    created_at: new Date(),
    updated_at: new Date(),
    data: {text: 'what '}
    };
    @Output() sendMessage = new EventEmitter();
    @ViewChild('messageInput')
    public messageInput: ElementRef;
    constructor(private messagesService: MessagesService){}
    public ngAfterContentInit() {
        autosize(this.messageInput.nativeElement);
    }

    public showStickers() {
        this.hidden = false;
        this.display = 'flex';
    }

    public hideStickers(e) {
        this.hidden = e.hidden;
        setTimeout(() => {
            this.display = e.display;
        }, 300);
    }



    public sendTextMessage(){
        //console.log(this.messageInput.nativeElement.value);
        this.sendMessage.emit({type: 'text', data:{text: this.messageInput.nativeElement.value}})
        this.messageInput.nativeElement.value = '';
       // this.messagesService.addMessage(this.message)
    }
    public sendStickerMessage(e){
       console.log(e)
        this.sendMessage.emit(e)
    }
}
